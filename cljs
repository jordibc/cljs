#!/usr/bin/env bb

;; Create all that is needed to start coding and having fun with
;; clojurescript (including react and processing/p5js.

;; To edit this with cider, run "bb --nrepl-server 9999" and in emacs
;; run cider-connect-clj. Or "M-x babashka"!

(ns cljs
  (:require [babashka.fs :as fs]
            [clojure.java.shell :as shell]))

(defn sources
  "Return a map with source file names and their content"
  [pname use-quil? use-mui?]
  {"public/index.html"
   (str
    "<!doctype html>
<html>
  <head>
    <meta charset=\"utf-8\" />
    <title>" pname "</title>
  </head>
  <body>
    <div id=\"root\"></div>"
    (if use-quil? "\n    <div id=\"canvas\"></div>")
    "
    <script src=\"./js/main.js\"></script>
  </body>
</html>
")
   "shadow-cljs.edn"
   (str
    "{:source-paths
 [\"src\"]

 :dependencies
 [[reagent/reagent \"RELEASE\"]"
    (if use-quil? "\n  [quil/quil \"RELEASE\"]")
    (if use-mui? "\n  [arttuka/reagent-material-ui \"5.10.1-0\"]")
    "]

 :dev-http
 {8080 \"public\"}

 :js-options
 {:anon-fn-naming-policy :unmapped} ; to see component names when debugging

 :builds
 {:app
  {:target :browser
   :devtools {:after-load app/main}
   :modules {:main {:init-fn app/main}}}}}

;; If using mui, you may want to use \"5.10.1-0\" instead of \"RELEASE\".
;; To prepare it for a website, you can run:
;;   npx shadow-cljs release app
;; and then in public/ just run make to upload to bb.
")
   "package.json"
   (str
   "{
  \"name\": \"" pname "\",
  \"version\": \"0.1\",
  \"private\": true,
  \"devDependencies\": {
    \"shadow-cljs\": \"2.18.0\"
  },
  \"dependencies\": {"
   (if use-quil? "
    \"p5\": \"^1.4.1\",
    \"quil\": \"^1.1.30\",")
   (if use-mui? "
     \"@mui/material\": \"5.6.2\",")
   "
    \"react\": \"^18.0.0\",
    \"react-dom\": \"^18.0.0\""
   "
  }
}
")
   "src/app.cljs"
   (str
    "(ns app
  (:require [reagent.core :as r]
            [reagent.dom :as rdom]"
    (if use-quil? "
            [quil.core :as q]")
    (if use-mui? "
            [reagent-mui.components :as mui]")
    "))

"
    (if use-quil? "(defn draw []
  (q/stroke (q/random 255))
  (q/stroke-weight (q/random 10))
  (q/fill (q/random 255))

  (let [r 20
        x (q/random (q/width))
        y (q/random (q/height))]
    (q/ellipse x y r r)))
")
    "
(defn main []"
    (if use-quil? "
  (q/sketch
   :host \"canvas\"
   :draw draw
   :size [500 500])")
    "
  (rdom/render
   [:div"
    (if use-mui? "
    [mui/button {:on-click #(js/alert \"Surprise!\")} \"Click me\"]")
"
    [:h1 \"Hi!\"]]
   (js/document.getElementById \"root\")))
")
   "public/Makefile"
   (str
    "# This will upload the full app as a simple javascript to bb.
upload:
	ssh bb mkdir -p /var/www/metamagical.org/" pname "/js
	scp index.html bb:/var/www/metamagical.org/" pname "
	scp js/main.js bb:/var/www/metamagical.org/" pname "/js
")})


(defn write-source-files
  "Write all the files with their content returned by \"sources\""
  [base-path pname use-quil? use-mui?]
  (doseq [[path content] (sources pname use-quil? use-mui?)]
    (let [dirname (str base-path "/" (fs/parent path))
          fname   (str base-path "/" path)]
      (fs/create-dirs dirname) ; mkdir -p
      (spit fname content))))


(defn show-output [args]
  "Show output returned from a call to shell/sh"
  (let [{:keys [exit out]} args]
    (if (= 0 exit)
      (println out)
      (println (str "Error:\n" args)))))


(defn create-project [pname use-quil? use-mui?]
  (let [base-path (str "/home/jordi/projects/personal/timeless/wip/" pname)]
    (println "Writing source files...")
    (write-source-files base-path pname use-quil? use-mui?)

    (println "Installing npm base packages...")
    (show-output (shell/sh "yarn" "install" :dir base-path))

    (println "Launching emacs with cider...")
    (show-output (shell/sh "emacs" "src/app.cljs"
                           "--eval" "(cider-jack-in-cljs '())"
                           :dir base-path))
    (println "Done!")))


(defn main []
  (let [[pname use-quil? use-mui?] *command-line-args*]
    (if (and pname use-quil? use-mui?)
      (create-project pname (= "y" use-quil?) (= "y" use-mui?))
      (println (str "usage: " *file* " <project name> "
                    "<y/n for using quil> "
                    "<y/n for using mui>")))))

(main)
