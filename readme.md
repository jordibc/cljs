# cljs

`cljs` is a command line to get me started with a clojurescript
project. It is done in [babashka](https://babashka.org/).

To use it:

```
usage: cljs <project name> <y/n for using quil> <y/n for using mui>
```

In addition to the script, I have the following settings in my
`.emacs` to use [cider](https://docs.cider.mx/cider/index.html) with
[shadow-cljs](https://shadow-cljs.github.io/docs/UsersGuide.html) and
making it watch my default build name (`app`):

```lisp
;; No annoying help banner for cider
(setq cider-repl-display-help-banner nil)

;; Use shadow-cljs and run :app by default for cider
(setq cider-default-cljs-repl 'shadow
      cider-shadow-default-options "app"
      cider-shadow-watched-builds '("app"))
```

And I add this function too so I can comfortably edit `cljs` or any
other babashka script:

```lisp
;; Launch a cider session for babashka
(defun babashka ()
  (interactive)
  (start-process "babashka nrepl" "babashka nrepl" "bb" "--nrepl-server" "9999")
  (cider-connect-clj '(:host "localhost" :port 9999)))
```
